///////////////////////////////
// FUNZIONI UTILI TESI       //
///////////////////////////////

// la libreria va inclusa dopo aver definito LEN e ALPHA
// se necessari

#include <stdio.h>
#include <math.h>
#include <stdlib.h>  // alcune cose tra cui atof
#include <gsl/gsl_sf.h>  // include la GNU scientific library
#include <complex.h>
#include <gsl/gsl_fft_complex.h>

// costanti utili che nel C99 non sono definite
// compaiono in gsl_fft_complex quindi se uso quella non servono
#ifndef M_PI
#define M_PI           (3.14159265358979323846)
#endif
#ifndef M_E
#define M_E            (2.71828182845904523536)
#endif
#ifndef PHI
#define PHI            (0.3819660112501051)
#endif
// costante di quantizzazione, per l'oscillatore armonico è 1
#ifndef ALPHA
#define ALPHA (1.0)
#endif

// lunghezza dei vettori per Numerov e altre cose
#ifndef LEN
#define LEN (16384)
#endif

// numero di componenti di Fourier nelle FFT
#ifndef N
#define N (1024)
#endif

// se f è la funzione complessa, sta cosa si usa REAL(f, i) per l'i-mo valore reale
// serve per mantenere un po' di sanità mentale con le convenzioni di gsl_fft
#define REAL(z,i) ((z)[2*(i)])
#define IMAG(z,i) ((z)[2*(i)+1])
#define ABS(z,i) (hypot((z)[2*(i)+1], (z)[2*(i)]))
#define P(z) creal(z), cimag(z) // per stampare i complessi, si usa in printf


////////////////////
// NEWTON-RAPHSON //
////////////////////
double complex NR(double complex (*f)(double complex, void *), void *p, double complex x0, double dx, double complex h, int verbose)
{
    // Il valore iniziale va divinato in qualche modo, se non è divinato
    // abbastanza bene si rompe.
    double complex x = x0;
    double complex x_old = x;
    // in realtà non è *proprio* NR, dato che stiamo approssimando la tangente
    // con la secante, quindi h va bene anche abbastanza grande, tipo 0.1

    int count = 0;

    do
    {
        count++;
        x_old = x;

        x = x - 2*h*f(x, p)/(f(x+h, p)-f(x-h, p));

        if (verbose)
            printf("# E = %g+i%g \t |f(E)| = %g \t dE = %g \n", P(x), cabs(f(x, p)), cabs(x-x_old));
    }
    while(cabs(f(x, p))>dx);
    // while(cabs(x-x_old)>dx || count < 10);
    // while(cabs(x-x_old)>dx || cimag(x) > 0 || count<10);

    return x;
}


/////////////
// NUMEROV //
/////////////
// I Numerov nelle due direzioni, potrebbero essere messi insieme ma così è più chiaro. Nel vettore u ci devono già essere le BC
void numerov_forw(double complex (*f)(int , void *), void *par, double *x, double complex *u, double complex h) {
    for (int i = 1; i < LEN-1; ++i)
    {
        u[i+1] = ((2+5./6*pow(h, 2)*f(i, par))*u[i] -
                    (1-1./12*pow(h, 2)*f(i-1, par))*u[i-1]) /
                    (1-1./12*pow(h, 2)*f(i+1, par));
    }
}

void numerov_back(double complex (*f)(int , void *), void *par, double *x, double complex  *u, double complex h) {
    for (int i = LEN-2; i > 0; --i)
    {
        u[i-1] = ((2+5./6*pow(h, 2)*f(i, par))*u[i] -
                    (1-1./12*pow(h, 2)*f(i+1, par))*u[i+1]) /
                    (1-1./12*pow(h, 2)*f(i-1, par));
    }
}


///////////////////////////////////////////
// POTENZIALE, INTERFACCIA VERSO NUMEROV //
///////////////////////////////////////////
// V_wrap vuole la posizione in cui calcolare la sorgente e un puntatore ad una
// struttura del tipo par_V
struct par_V {
    double complex E; double l; double complex *V; double *x;
};

double complex source(int i, double complex E, double l, double complex *V, double *x) {
    // "sorgente" dell'eq Schrodinger nella forma in cui la risolve
    // Numerov, controllo che la coordinata sia radiale
    if (x[i]>0)
    {
    return  V[i] / ALPHA
            + l*(l+1)/pow(x[i], 2)
            - E/ALPHA;
    }

    else
    {
        // printf("Il raggio è %g nella sorgente, che è successo?\n", x[i]);
        return 0;
        // exit(0);
    }
}

double complex V_wrap(int i, void *par) {
    // wrapper per passare il potenziale a Numerov
    struct par_V *p = (struct par_V *) par;
    return source(i, p->E, p->l, p->V, p->x);
}


//////////////////////////////////
// DISCONTINUITÀ DELLE DERIVATE //
//////////////////////////////////
struct par_dE
{
    // "sorgente" per Numerov
    double complex (*f)(int, void *);
    double l; double complex *V; double *x; double complex x_conf; double complex *phi;
    // condizioni iniziali vicino e lontano
    double complex *BC1; double complex *BC2;
};

double complex diff_derivate(double complex E, double l, double complex (*f)(int, void *), double complex *V, double *x, double x_conf, double complex *phi, double complex *BC1, double complex *BC2) {
    // integra con numerov la funzione d'onda e calcola la discontinuità delle
    // derivate nel punto x_conf, *phi è la funzione d'onda che corrisponde
    // alla discontinuità dE, la funzione che gli passo è V_wrap

    // struttura per la sorgente di Numerov
    struct par_V t = {.E = E, .l = l, .V = V, .x = x};
    struct par_V *par = &t;

    // indice nel vettore ascisse che assumerà x_conf
    int conf;
    for (int i = 0; i < LEN; ++i)
    {
        if ((-x_conf+x[0]+i*(x[LEN-1]-x[0])/(LEN-1)) *
            (-x_conf+x[0]+(i+1)*(x[LEN-1]-x[0])/(LEN-1)) < 0)
        {
            conf = i;
            break;
        }
    }
    // printf("# x_conf = %g, uso x[%d] = %g\n", x_conf, conf, x[conf]);

    // vettore che ospiterà i valori della funzione d'onda
    double complex ub[LEN], uf[LEN];
    double complex ur[LEN], ul[LEN];

    // step per numerov, così copro la distanza da x_min a x_max
    double dx = fabs(x[1]-x[0]);
    double complex dE;

    // Integrazione forward
    uf[0] = BC1[0];
    uf[1] = BC1[1];
    numerov_forw(f, par, x, uf, dx);


    // Integrazione backward
    ub[LEN-2] = BC2[0];
    ub[LEN-1] = BC2[1];
    numerov_back(f, par, x, ub, dx);


    // Raccordo C0 le due funzioni
    for (int i = 0; i <= conf+2; ++i) {
        ul[i] = uf[i]/uf[conf];
        // printf("%g\t%g\n", x[i], ul[i]);
    }

    // ramo destro
    for (int k = conf-2; k < LEN; ++k) {
        ur[k] = ub[k] * ul[conf]/ub[conf];
        // printf("%g\t%g\n", x[k], ur[k]);
    }

    // costruisco la funzione d'onda effettiva
    for (int i = 0; i < LEN; ++i)
    {
        phi[i] = i<conf? ul[i]:ur[i];
    }

    // il conto è: ur'-ul' nel punto x[conf]
    dE = (ur[conf+1] + ul[conf-1] - 2*ur[conf] - pow(dx, 2)*f(conf, par))/dx;

    // printf("# ul[%d] = %g\tur[%d] = %g\t\n", conf, ul[conf], conf, ur[conf]);
    // printf("# E = %g\tx_conf = %.6lf\tdE = %g\n", E, x[conf], dE);
    return dE;
}

double complex diff_derivate_wrap(double complex E, void *p)
{
    struct par_dE *der = (struct par_dE *) p;

    // in modo che la condizione iniziale lontano abbia la fase giusta
    double complex k = csqrt(E/ALPHA);
    // condizioni iniziali lontano
    der->BC2[0] = cexp(I*k*der->x[LEN-2]);
    der->BC2[1] = cexp(I*k*der->x[LEN-1]);

    // printf("prova: %g+i%g %g+i%g\n", P(der->BC2[0]), P(der->BC2[1]));
    // exit(0);

    return diff_derivate(E, der->l, der->f, der->V, der->x, der->x_conf, der->phi, der->BC1, der->BC2);
}


///////////
// ALTRO //
///////////
// restituisce la norma prima della normalizzazione
double complex cnormalizza(double complex dx, double complex *phi)
{
    if (LEN % 2)
        printf("N deve essere pari, altrimenti simpson non funziona\n");

    // calcolo la norma con la regola di simpson
    double complex integ =  dx/3. * (cabs(phi[0])*cabs(phi[0])+cabs(phi[LEN-1])*cabs(phi[LEN-1]));
    for (int k = 1; k < LEN; k += 2)
        integ += dx/3. * 4. * cabs(phi[k])*cabs(phi[k]);
    for (int k = 2; k < LEN-1; k += 2)
        integ += dx/3. * 2. * cabs(phi[k])*cabs(phi[k]);

    // normalizzo
    for (int i = 0; i < LEN; ++i)
        phi[i] = phi[i]/sqrt(integ);

    return sqrt(integ);
}

//trova il massimo valore in un vettore, a partire dall'indice start incluso
double max_val(int start, double *v)
{
    double max;
    for (int i = start; i < LEN; ++i)
    {
        max = v[i]>max? v[i] : max;
    }
    return max;
}


//integro un vettore campionato
double complex trapezi(double complex dx, double complex *f, int min, int max)
{
    double complex integ = 0.5*(f[min]+f[max]);

    for (int i = min+1; i < max; ++i)
        integ += f[i];

    return dx*integ;
}


double complex simpson(double complex dx, double complex *phi)
{
    if (LEN % 2)
        printf("N deve essere pari, altrimenti simpson non funziona\n");

    // calcolo la norma con la regola di simpson
    double complex integ =  dx/3 * (phi[0]+phi[LEN-1]);

    for (int k = 1; k < LEN; k += 2)
        integ += dx/3 * 4 * phi[k];
    for (int k = 2; k < LEN-1; k += 2)
        integ += dx/3 * 2 * phi[k];

    return integ;
}
