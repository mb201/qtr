function NR(f, x, dx, h, p...; verbose=false)
    # così controlla la qualità dello zero e non la precisione
    count = 1
    x_old = x + 1
    x_new = x
    while abs(x_new-x_old) > dx
        x_old = x_new
        x_new = x_old - 2h*f(x_old, p...)/(f(x_old+h, p...)-f(x_old-h, p...))
        # imaginary trick, non funziona con potenziali a tratti
        # x_new = x_old - h*f(x_old, p...)/imag(f(x_old+h*im, p...))
        if verbose
            println(x, "\t",abs(f(x, p...)))
        else
            write(".")
        end
        count += 1
        if count > 15
            error("Non è stata raggiunta la convergenza in 15 iterazioni")
        end
    end
    return x_new
end

function norm(dx, u)
    # norma di u
    res = 0.5*(abs2(u[1])+abs2(u[end]))
    for i in [2:length(u)-1]
        res += abs2(u[i])
    end
    return sqrt(dx*res)
end
