#define ALPHA (1e-1)    // Misura quanto è quantistico il tutto, è h**2/2mV0a**2
                      // se è MOLTO piccolo (1e-12) devono venire i livelli della buca infinita E = alpha*n^2*pi^2
#define LEN   (1000)   // Lunghezza del vettore per Numerov

#include "funzioni.c"

/////////////////////////////////////////////////////////////////
// Funziona bene con ALPHA = 1e-12, h=1e-21, dx=1e-9, a=1, b=2 // BUCA INFINITA
/////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////
// Funziona decentemente con A=0.1 h=1e-4, dx=1e-9, a=1, b=2, n=1//  BUCA FINITA
///////////////////////////////////////////////////////////////////

// caso analitico della sfera cava, calcolo le energie complesse con la
// formula che viene dal calcolo con carta e penna. Trovo gli zeri con NR. La
// condizione sulle energie è stata scelta tra tutte quelle possibili perché
// cosi si riesce a calcolare, quella col reciproco scazza perché c'è
// l'esponenziale e va in overflow subito.

// il gradino va da 'a' fino a 'b' e a=1 è l'unità ridotta di lunghezza

// se la buca è troppo poco profonda, non ci sono più stati legati. È diverso
// dal caso della buca finita in cui uno stato si trova sempre, perché qui
// impongo che lo stato sia *dispari* con le condizioni al contorno.

struct par_en {
    double a; double b; double V0;
};

double complex energia(double complex E, double a, double b, double V0) {
    double c = b - a;
    double complex res;

    double complex k  = csqrt(E/ALPHA);
    // ci va il modulo? dall'equazione di schrodinger viene così, ma anche col modulo ha senso
    double complex mu = csqrt((V0-E)/ALPHA);

    // calcolata con carta e penna è vera anche quella coi reciproci, che magari è più facile da calcolare
    // res = cexp(2*mu*c) - (1+I*k/mu)/(1-I*k/mu) * (csin(k*a) - k/mu*ccos(k*a))/(csin(k*a) + k/mu*ccos(k*a));
    res = cexp(-2*mu*c) - (1-I*k/mu)/(1+I*k/mu) * (csin(k*a) + k/mu*ccos(k*a))/(csin(k*a) - k/mu*ccos(k*a));

    return res;
}

double complex en_wrap(double complex E, void *par) {
    // wrapper per passare la quantizzazione a NR
    struct par_en *p = (struct par_en *) par;
    return energia(E, p->a, p->b, p->V0);
}

int main(int argc, char const *argv[])
{
    // energie della sfera cava

    // la buca è sempre larga 1, cambio lo spessore del muro
    double a = 1;
    double b = 2;

    // è l'unità ridotta di energia
    double V0 = 1;

    // stima iniziale dell'energia, sono quelli della buca infinita
    double n = 1; // = atof(argv[2]); // livello energetico della buca infinita, usato nella stima iniziale
    double complex E_att = ALPHA*M_PI*M_PI*n*n;
    double complex E;

    struct par_en ten= {.a = a, .b = b, .V0 = V0};
    struct par_en *pen = &ten;

    // parametri per NR
    double dx = 1e-14;
    double h = 1e-4;

    for (n = 1; n < 3; ++n)
    {
        E_att = ALPHA*M_PI*M_PI*n*n;
        E = NR(en_wrap, pen, E_att, dx, h, 1);
        printf("# Energia calcolata: E = %g+i%g\n", P(E));
        printf("# Energia attesa (buca infinita): %g\n", creal(E_att));
        // printf("%g & %g+i%g \\\\ \n", creal(E_att), P(E));
    }
        printf("# a = %g    b = %g    ALPHA = %g\n", a, b, ALPHA);
        printf("\n");

    return 0;
}
