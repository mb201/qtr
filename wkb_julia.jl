##########################################
# trova gli stati metastabili con la WKB #
# ########################################

using PyPlot
using Roots   # per trovare gli zeri

include("funzioni.jl")

# costanti
ħ  = 1.05457e-34
eV = 1.60217e-19
me = 9.10938e-31
md = 0.02me
s  = 5e-9

####################################################################
# Parametri del problema
pot           = 10
V0            = 0.5                       # in eV
q             = 3                         # intercetta del potenziale
E             = -1.5 + 0.01im + q         # stima iniziale
a, b          = 3, 4
x_min, x_max  = 0, 10
LEN           = 1000
xc            = 3.9 #(a+b)/2
# dovrebbe essere 0.008 < ζ < 0.08
const ζ = ħ^2/(2*md*V0*eV*s^2); println(ζ)
x             = linspace(x_min, x_max, 256)
# boundary conditions
k             = sqrt(complex(E/ζ))
BCf           = [1e-21, 2e-21]
BCb           = [exp(im*k*x[end-1]), exp(im*k*x[end])]
# parametri ricerca energia
dE = 1e-9
h  = 1e-4
#####################################################################

# regola di quantizzazione semiclassica
function quant(E, V, n)
    # k e  β sono la stessa cosa a meno di una im: β = im*k
    k(x) = sqrt(complex(E-V(x))/ζ)
    β(x) = sqrt(complex(V(x)-E)/ζ)

    # r0 è il punto di inversione, calcolato con la ***parte reale*** di E
    r0 = 3 #fzero(x -> V(x)-real(E), 0.1, 1.99)
    r1 = 4  #fzero(x -> V(x)-real(E), 0.1, 0.3)
    r2 = fzero(x -> V(x)-real(E), 4.1, 10)
    # println("r_0 = ", r0, "  r_1 = ", r1, "  r_2 = ", r2)

    J = quadgk(k, r0, r1)[1]
    K = quadgk(β, r1, r2)[1]
    # println("J = $J \t K = $K")

    return J - (n+1/2)*pi - 0.5im*log((1-0.25*exp(-2K))/(1+0.25*exp(-2K)))
end

function wavefun(E, V, n)
    # k e  β sono la stessa cosa a meno di una im: β = im*k
    k(x) = sqrt(complex(E-V(x))/ζ)
    β(x) = sqrt(complex(V(x)-E)/ζ)

    # r0 è il punto di inversione, calcolato con la ***parte reale*** di E
    r0 = 3 #fzero(x -> V(x)-real(E), 0.1, 1.99)
    r1 = 4  #fzero(x -> V(x)-real(E), 0.1, 0.3)
    r2 = fzero(x -> V(x)-real(E), 4.1, 10)
    println("r_0 = ", r0, "  r_1 = ", r1, "  r_2 = ", r2)
    # println("r_0 = ", r0)

    J = quadgk(k, r0, r1)[1]
    K = quadgk(β, r1, r2)[1]
    # println("J = $J \t K = $K")

    function ϕ(r)
        # definisco i vari tratti
        ϕ1(r) = 1/(2*sqrt(β(r))) * exp(-quadgk(β, r, r0)[1])
        ϕ2(r) = 1/sqrt(k(r))     * sin(quadgk(k, r0, r)[1] + pi/4)
        ϕ3(r) = (-1)^n/(2*sqrt(β(r))) * 1/sqrt(1-1/16*exp(-4K)) *
                (exp(-quadgk(β, r1, r)[1]) + 0.5im * exp(-2K) * exp(quadgk(β, r1, r)[1]))
        ϕ4(r) = (-1)^n/(2*sqrt(k(r))) * exp(im*pi/4 - K) * exp(im*quadgk(k, r2, r)[1])

        # per le discontinuità nette
        epsilon = 1e-6
        A =   ϕ1(r0-epsilon)/ϕ2(r0+epsilon)
        B = A*ϕ2(r1-epsilon)/ϕ3(r1+epsilon)
        C = B*ϕ3(r2-epsilon)/ϕ4(r2+epsilon)

        if  r < r0
            ϕ1(r)
        elseif r0 < r < r1
            ϕ2(r)*A
        elseif r1 < r < r2
            ϕ3(r)*B
        else
            ϕ4(r)*C
        end
    end

    return ϕ
end

# parametri vari del problema
l = 0 # momento angolare
n = 0 # numero quantico

# potenziale
# V(x) = (x>a && x<b)? 1 : 0 + ζ*(l+0.5)^2/x^2
# V(x) = 1/(0.5*sqrt(2pi)) * exp(-(x-1.5)^2/0.25) + ζ*(l+0.5)^2/x^2
# V(x) = -0.15/V0 * x - ((x>a && x<b)? 1 : 0) + q
V(x) = -(pot+0.8)*5/(360*V0) * x - ((x>a && x<b)? 1 : 0) + q

grid(true)
xlim(x_min,x_max)
plot(x, map(V, x))

# cerco lo zero
E = NR(quant, E, dE, h, V, n, verbose=false)
println("E = $E")

# tunneling rate
ni = -2*imag(E)*V0*eV/(2*pi*ħ) * 1e-12 # così viene in 1/ps
println("rate = $ni 1/ps")

# costruisce e disegna la funzione d'onda
# function grafica(E, V, n)
v = wavefun(E, V, n)
A = sqrt(quadgk(x -> abs2(v(x)), x_min,x_max)[1])
y = map(v, x)/A
plot(x, real(y+E), x, E*ones(length(x)))
# end

# grafica(E, V, n)
