using PyPlot

include("funzioni.jl")

####################################################################
# Parametri del problema
E             = 1.7
x_min, x_max  = -5, 5
LEN           = 30000
xc            = 0.1
# quantumness, per ho è definita 1
const ζ = 1
x             = linspace(x_min, x_max, LEN)
# boundary conditions
k             = sqrt(complex(E/ζ))
BCf           = [1e-21, 2e-21]
BCb           = [2e-21, 1e-21]
# parametri ricerca energia
dE = 1e-6
h  = 1e-4
#####################################################################

function numerov(f, x, BC, conf::Int, p...; verse="any")
    h = x[2]-x[1]
    N = length(x)
    u = complex(zeros(N))

    if verse == "forw"
        u[1] = BC[1]
        u[2] = BC[2]
        for i in [3:conf+2]
            u[i+1] = ((2+5/6*h^2*f(x[i],  p...))*u[i] -
                     (1-1/12*h^2*f(x[i],  p...))*u[i-1]) /
                     (1-1/12*h^2*f(x[i+1],p...));
        end
    elseif verse == "back"
        u[N]   = BC[2]
        u[N-1] = BC[1]
        for i in [N-1:-1:conf-2]
            u[i-1] = ((2+5/6*h^2*f(x[i]  ,p...))*u[i] -
                      (1-1/12*h^2*f(x[i+1],p...))*u[i+1]) /
                      (1-1/12*h^2*f(x[i-1],p...));
        end
    else
        error("The verse must be either 'forw' or 'back'")
    end

    return u
end

function Δ(E, V, l, x, conf::Int, BCf, BCb; build_wave=false)
    N = length(x)
    h = x[2] - x[1]

    # controlla che la coordinata sia radiale, altrimenti fa 0
    # f(x) = x > 0 ? (V(x)-E)/ζ + l*(l+1)/x^2 : 0
    f(x) = (V(x)-E)/ζ

    uf = numerov(f, x, BCf, conf, verse="forw")
    ub = numerov(f, x, BCb, conf, verse="back")

    ul = uf/uf[conf]
    ur = ub * ul[conf]/ub[conf]

    u = complex(zeros(N))
    for i in [1:N]
        u[i] = i<conf ? ul[i] : ur[i]
    end

    # ylim(0, 1.5)
    # xlim(-5, 5)
    # sleep(0.5)
    # plot(x, u)

    if build_wave
        return u
    else
        dE = (ur[conf+1] + ul[conf-1] - 2*ur[conf] - h^2*f(x[conf]))/h
        return dE
    end
end

function find_in(x, xc)
    for i in [1:length(x)]
        if (x[i]-xc)*(x[i+1]-xc)<0
            println("x_c = $xc = x[$i]")
            return i
        end
    end
    error("Il valore cercato non è nel range")
end

# momento angolare
l = 0

# griglia posizioni e condizioni iniziali
dx = x[2]-x[1]
conf = find_in(x, xc)

# potenziale
V(x) = x^2

# disegno il potenziale e la griglia
grid(true)
ylim(0, 2)
xlim(x_min, x_max)
plot(x, map(V, x))

# trovo gli stati legati
E = NR(Δ, E, dE, h, V, l, x, conf, BCf, BCb, verbose=false)
println("E = $E")
