# README #

This repository contains the code written for the thesis in Physics "Numerical computation of quantum tunnelling rates", discussed by M.B. on 2014 sep 26th at the university of Trento.

# C files #
The C sources should be compiled with the C99 flag.  By default, GSL is linked, but it is not necessary: this can be avoided commenting the relative line in "funzioni.c". So the compilation command should be something like

    $ gcc file_name.c -o file_name.out -lm -Wall -std=c99 -lgsl -lgslcblas
    $ ./file_name.out

if the program requires any argument it will print an error message, asking the user to provide it.

# Julia files #
If you have a working Julia install on your computer, the provided files will produce the rate and solutions discussed in the last chapter of the thesis, for a potential difference across the QD of 10V. PyPlot is required. In a terminal run

    $ julia
    julia> include("file.jl") 

to obtain the plot. This will give interactive access to all the parameters of the problem.