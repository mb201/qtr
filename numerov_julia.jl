using PyPlot

# Numerov funziona sia con HO che con la sfera cava

include("funzioni.jl")

# costanti
ħ  = 1.05457e-34
eV = 1.60217e-19
me = 9.10938e-31
md = 0.02me
s  = 5e-9

####################################################################
# Parametri del problema
pot           = 10
V0            = 0.5                       # in eV
q             = 3                      # intercetta del potenziale
E             = -1.5 + 0.01im + q         # stima iniziale
a, b          = 3, 4
x_min, x_max  = 0, 10
LEN           = 30000
xc            = 3.9 #(a+b)/2
# dovrebbe essere 0.008 < ζ < 0.08
const ζ = ħ^2/(2*md*V0*eV*s^2); println(ζ)
x             = linspace(x_min, x_max, LEN)
# boundary conditions
k             = sqrt(complex(E/ζ))
BCf           = [1e-21, 2e-21]
BCb           = [exp(im*k*x[end-1]), exp(im*k*x[end])]
# parametri ricerca energia
dE = 1e-9
h  = 1e-4
#####################################################################

function numerov(f, x, BC, conf::Int, p...; verse="any")
    h = x[2]-x[1]
    N = length(x)
    u = complex(zeros(N))

    if verse == "forw"
        u[1] = BC[1]
        u[2] = BC[2]
        for i in [3:conf+2]
            u[i+1] = ((2+5/6*h^2*f(x[i],  p...))*u[i] -
                     (1-1/12*h^2*f(x[i],  p...))*u[i-1]) /
                     (1-1/12*h^2*f(x[i+1],p...));
        end
    elseif verse == "back"
        u[N]   = BC[2]
        u[N-1] = BC[1]
        for i in [N-1:-1:conf-2]
            u[i-1] = ((2+5/6*h^2*f(x[i]  ,p...))*u[i] -
                      (1-1/12*h^2*f(x[i+1],p...))*u[i+1]) /
                      (1-1/12*h^2*f(x[i-1],p...));
        end
    else
        error("The verse must be either 'forw' or 'back'")
    end

    return u
end

function Δ(E, V, l, x, conf::Int, BCf, BCb; build_wave=false)
    N = length(x)
    h = x[2] - x[1]

    # controlla che la coordinata sia radiale, altrimenti fa 0
    f(x) = x > 0 ? (V(x)-E)/ζ + l*(l+1)/x^2 : 0

    # soluzione libera all'infinito
    k = sqrt(complex(E/ζ))
    BCb = [exp(im*k*x[N-1]), exp(im*k*x[N])]

    uf = numerov(f, x, BCf, conf, verse="forw")
    ub = numerov(f, x, BCb, conf, verse="back")

    ul = uf/uf[conf]
    ur = ub * ul[conf]/ub[conf]

    u = complex(zeros(N))
    for i in [1:N]
        u[i] = i<conf ? ul[i] : ur[i]
    end

    # ylim(-0.1, 1.3)
    # xlim(5, 25)
    # sleep(0.1)
    # plot(x, u)

    if build_wave
        return u
    else
        dE = (ur[conf+1] + ul[conf-1] - 2*ur[conf] - h^2*f(x[conf]))/h
        return dE
    end
end

# Z = [3,6,9,12,15] # voglio trovare l'indice del numero più vicino a 10
# Z[indmin(abs(Z .- 10))] # indmin trova
function find_in(x, xc)
    for i in [1:length(x)]
        if (x[i]-xc)*(x[i+1]-xc)<0
            println("x_c = $xc = x[$i]")
            return i
        end
    end
    error("Il valore cercato non è nel range")
end

# momento angolare
l = 0

# griglia posizioni e condizioni iniziali
dx = x[2]-x[1]
conf = find_in(x, xc)

# potenziale
V(x) = -(pot+0.8)*5/(360*V0) * x - ((x>a && x<b)? 1 : 0) + q

# disegno il potenziale e la griglia
grid(true)
# ylim(-0.1, 2)
xlim(x_min, x_max)
plot(x, map(V, x))

# trovo gli stati legati
E = NR(Δ, E, dE, h, V, l, x, conf, BCf, BCb, verbose=false)
println("E = $E")

# torno alle unità  SI
ni = -2*imag(E)*V0*eV/(2*pi*ħ) * 1e-12 # così viene in 1/ps
println("rate = $ni 1/ps")

# per disegnare la funzione d'onda
k = sqrt(complex(E/ζ))
BCb = [exp(im*k*x[end-1]), exp(im*k*x[end])]
u = Δ(E, V, l, x, conf::Int, BCf, BCb, build_wave=true)
u = u/norm(dx, u)

PyPlot.plot(x, ones(length(x))*E)
PyPlot.plot(x, real(u+E))
