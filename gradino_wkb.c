#define ALPHA (1e-1)    // Misura quanto è quantistico il tutto, è h**2/2mV0a**2
#define LEN   (50000)   // Lunghezza del vettore per Numerov

#include "funzioni.c"

/////////////////
// ATTENZIONE! //
/////////////////

// La WKB funziona solo se:
//      * il numero quantico è alto (così k >> 1/a), ma a volte funziona anche per n piccoli
//      * il potenziale varia lentamente (se è piatto a tratti va bene)
//      * la larghezza è più piccola dell'energia reale

void calcola_k(double complex E, double complex *V, double complex *k)
{
    // nel dubbio lo calcolo su tutto il range
    for (int i = 0; i < LEN; ++i)
        k[i] = csqrt((E-V[i])/ALPHA);
}

void calcola_beta(double complex E, double complex *V, double complex *beta)
{
    // in realtà serve solo dove è reale, ma non fa niente
    for (int i = 0; i < LEN; ++i)
        beta[i] = csqrt((V[i]-E)/ALPHA);
}

//////////////////////////////////////
// Condizione di quantizzazione WKB //
//////////////////////////////////////

struct par_q {
    double n; double complex *V; double *x;
};


double complex quantizzazione(double n, double *x, double complex *V, double complex E)
{
    // k è tra i primi due punti di inversione e b tra quelli dopo
    // cosa sono i punti di inversione se l'energia è complessa?
    // probabilmente vanno calcolati con la parte reale dell'energia

    double complex J, K;
    double dx = fabs(x[1]-x[0]);
    double complex res;
    int index_x1, index_x2, index_x3;

    index_x1 = index_x2 = index_x3 = 0;

    double complex *k    = malloc(LEN * sizeof(double complex));
    double complex *beta = malloc(LEN * sizeof(double complex));

    calcola_k(E, V, k);
    calcola_beta(E, V, beta);

    // calcolo i punti di inversione
    for (int i = 0; i < LEN; ++i)
    {
        if ( (creal(V[i]-E)) * (creal(V[i+1]-E)) <0) {
            if (index_x1==0) {
                index_x1 = i;
                continue;
            }
            if (index_x2==0) {
                index_x2 = i;
                continue;
            }
            if (index_x3==0) {
                index_x3 = i;
                break;
            }
        }
    }

    if (index_x3==0){
        printf("# ci sono solo due punti di inversione, default x3 = x[LEN]\n");
        index_x3 = LEN-1;
    }

    // printf("# E = %g+i%g\n", P(E));
    // printf("# x[i_x1] = %g   ", x[index_x1]);
    // printf("x[i_x2] = %g     ", x[index_x2]);
    // printf("x[i_x3] = %g\n", x[index_x3]);

    // exit(0);

    J = trapezi(dx, k, index_x1, index_x2);
    K = trapezi(dx, beta, index_x2, index_x3);
    // printf("# J = %g+i%g K = %g+i%g\n", P(J), P(K));


    // res = J - (n+0.5)*M_PI - 0.25*I*cexp(-2*K); // K >> 1
    res = J - (n+0.5)*M_PI - 0.5*I*clog((1-0.25*cexp(-2*K))/(1+0.25*cexp(-2*K)));

    free(k);
    free(beta);

    return res;
}

double complex q_wrap(double complex E, void *par) {
    // wrapper per passare la quantizzazione a NR
    struct par_q *p = (struct par_q *) par;
    return quantizzazione(p->n, p->x, p->V, E);
}

void funzione_onda(double n, double *x, double complex *V, double complex E, double complex *psi)
{
    double complex K;
    double dx = fabs(x[1]-x[0]);
    int index_x1, index_x2, index_x3;

    index_x1 = index_x2 = index_x3 = 0;

    double complex *k = malloc(LEN * sizeof(double complex));
    double complex *beta = malloc(LEN * sizeof(double complex));

    calcola_k(E, V, k);
    calcola_beta(E, V, beta);

    // calcolo i punti di inversione
    for (int i = 0; i < LEN; ++i)
    {
        if ( (creal(V[i]-E)) * (creal(V[i+1]-E)) <0) {
            if (index_x1==0) {
                index_x1 = i;
                continue;
            }
            if (index_x2==0) {
                index_x2 = i;
                continue;
            }
            if (index_x3==0) {
                index_x3 = i;
                break;
            }
        }
    }

    if (index_x3==0){
        printf("# ci sono solo due punti di inversione, default x3 = x[LEN]\n");
        index_x3 = LEN-1;
    }

    printf("# E = %g+i%g\n", P(E));
    printf("# x[%d] = %g\n", index_x1, x[index_x1]);
    printf("# x[%d] = %g\n", index_x2, x[index_x2]);
    printf("# x[%d] = %g\n", index_x3, x[index_x3]);

    K = trapezi(dx, beta, index_x2, index_x3);

    printf("# E = %g+i%g n = %g K = %g+i%g\n", P(E), n, P(K));

    // exit(0);
    // core del potenziale
    printf("# Calcolo la funzione nella zona 1\n");
    for (int i = 0; i < index_x1; ++i)
        psi[i] =  1/(2*csqrt(beta[i])) * cexp(-trapezi(dx, beta, i, index_x1));

    // nella buca
    printf("# Calcolo la funzione nella zona 2\n");
    for (int i = index_x1; i < index_x2; ++i)
        psi[i] = 1/csqrt(k[i]) * csin(trapezi(dx, k, index_x1, i) + M_PI/4.);

    // sotto la barriera
    printf("# Calcolo la funzione nella zona 3\n");
    for (int i = index_x2; i <= index_x3; ++i)
        psi[i] = pow(-1, n)/(2*csqrt(beta[i])) * (
                cexp(-trapezi(dx, beta, index_x2, i)) +
                0.5 * I * cexp(-2*K) * cexp(trapezi(dx, beta, index_x2, i))
                );

    // oltre la barriera
    printf("# Calcolo la funzione nella zona 4\n");
    for (int i = index_x3; i < LEN; ++i)
        psi[i] = pow(-1, n) / (2*csqrt(k[i])) *
                cexp(I*M_PI/4)* cexp(-K) * cexp(I * trapezi(dx, k, index_x3, i));

    // la wkb si rompe nei punti di inversione. se i punti di inversione sono
    // gradini bisogna imporre la continuità a mano prima di normalizzare
    double complex cost_continu = psi[index_x2-1]/psi[index_x2+1];
    for (int i = index_x2+1; i < index_x3; ++i) {
        psi[i] *= cost_continu;
    }


    cost_continu = psi[index_x3-1]/psi[index_x3+1];
    for (int i = index_x3+1; i < LEN; ++i) {
        psi[i] *= cost_continu;
    }

    // printf("%g %g\n", creal(psi[index_x3-1]), creal(psi[index_x3]));
    // printf("%g %g\n", creal(psi[index_x3+1]), creal(psi[index_x3+2]));
    // printf("%g %g\n", creal(psi[index_x3+3]), creal(psi[index_x3+4]));
    // exit(0);

    free(k); free(beta);
}

int main(int argc, char const *argv[])
{

    // if (argc < 3)
    // {
    //     printf("Ci mancano degli argomenti\n");
    //     printf("Argomenti: n, stampa stato?(0|1)\n");
    //     exit(1);
    // }

    // momento angolare, usato per il calcolo del potenziale
    double l = 0;

    // Energia dello stato
    double complex E = 0;

    // griglia posizioni
    double x_min = 1e-21;
    double x_max = 3;
    double dx = fabs(x_max-x_min)/(LEN-1);
    double *x = malloc(LEN * sizeof(double));
        x[0] = x_min;
        for (int i = 1; i < LEN; ++i)
            x[i] = x[i-1] + dx;

    double dE;         // errore assoluto per NR
    double complex h;  // step per il calcolo della secante

    // potenziale in unità ridotte
    double a = 1;
    double b = 2;
    double complex *V = malloc(LEN * sizeof(double complex));
        for (int i = 0; i < LEN; ++i){
            V[i] = (x[i]>a && x[i]<b)? 1. : 0. +
                   ALPHA*(l+0.5)*(l+0.5)/pow(x[i], 2);
        }

    // livello energetico, con 0 è la prima risonanza
    double n = 0;

    struct par_q tq= {.n = n, .V = V, .x = x};
    struct par_q *pq = &tq;

    // condizione di quantizzazione semiclassica
    dE = 1e-9;
    E  = atof(argv[1]); //ALPHA*M_PI*M_PI*n*n;
    h  = 1e-4;

    printf("# quant: %g\n", cabs(quantizzazione(n, x, V, E)));

    E  = NR(q_wrap, pq, E, dE, h, 1);

    // exit(0);
    // funzione d'onda
    if (atoi(argv[2]))
    {
    double complex psi[LEN];
    funzione_onda(n, x, V, E, psi);
    cnormalizza(dx, psi);

    printf("# x |psi|^2\n");
    for (int i = 0; i < LEN; ++i)
        printf("%.15lf %.35lf\n", x[i], creal(psi[i]));
    }

    free(x); free(V);
    return 0;
}
