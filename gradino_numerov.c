#define ALPHA (1e-1)    // Misura quanto è quantistico il tutto, è h**2/2mV0a**2
#define LEN   (50000)   // Lunghezza del vettore per Numerov

#include "funzioni.c"

// struttura generale:
//      * calcolo il potenziale campionando
//      * al variare di l
//          * al variare di E
//              * calcolo la discontinuità nelle derivate delle funzioni
//                ottenute integrando con Numerov. Se ho incluso uno zero
//                faccio NR e trovo lo stato legato
//              * se sono richiesti gli stati eccitati continuo ad aumentare E

int main(int argc, char const *argv[])
{
    // if (argc == 1)
    // {
    //     printf("Ci mancano degli argomenti\n");
    //     printf("Argomenti: b, stampa_stato?\n");
    //     exit(1);
    // }

    double l = 0;

    // estremi del gradino
    double a = 1;
    double b = 2;

    // Energia dello stato
    double complex E = 0;

    // griglia posizioni
    double x_min = 0;
    double x_max = 3;
    double dx = fabs(x_max-x_min)/(LEN-1);
    double *x = malloc(LEN * sizeof(double));
        x[0] = x_min;
        for (int i = 1; i < LEN; ++i)
            x[i] = x[i-1] + dx;


    // potenziale
    double complex *V = malloc(LEN * sizeof(double complex));
        for (int i = 0; i < LEN; ++i){
            V[i] = (x[i]>=a && x[i]<=b) ? 1 : 0;
        }

    // x_conf dovrebbe essere in una zona classicamente permessa
    // per evitare divergenze durante l'integrazione
    double x_conf = (a+b)/2;

    // funzione d'onda
    double complex *phi = malloc(LEN * sizeof(double complex));

    // boundary conditions
    double complex *BC1 = malloc(2 * sizeof(double complex));
    double complex *BC2 = malloc(2 * sizeof(double complex));

    // struttura dati per la differenza delle derivate
    struct par_dE t = {.l = l,
                       .f = V_wrap,
                       .V = V,
                       .x = x,
                       .x_conf = x_conf,
                       .phi = phi,
                       .BC1 = BC1,
                       .BC2 = BC2};
    struct par_dE *p = &t;


    // for (E = 0.1; E < 1; E+=E_step)
    // {

        E = 0.538;
        double complex k = csqrt(E/ALPHA);
        // condizioni iniziali vicino
        BC1[0] = 0;
        BC1[1] = 1e-12;
        // condizioni iniziali lontano
        BC2[0] = cexp(I*k*x[LEN-2]);
        BC2[1] = cexp(I*k*x[LEN-1]);

        // E_step = 1e-4;

        // dE = diff_derivate(E, 0, V_wrap, V, x, x_conf, phi, BC1, BC2);
        // stampo la differenza delle derivate nel punto di raccordo
        // printf("%.15lf\t%.15lf\n", E, dE);

        // if (dE*diff_old<0)
        // {
            // eyecandy
            // printf("#\n# n = %d\t", n); printf("Intervallo energia: ");
            // printf(" \tE_old = %g E = %g \n", E_old, E);
            // printf("# \t\t\t\tdE_old = %g dE = %g\n#\n", diff_old, dE);

            // parametri per la ricerca dello 0
            double dEb = 1e-6;       // errore assoluto per NR
            double complex h = 1e-4; // step per il calcolo della secante

            double complex E0 = 0.538;//atoi(argv[3]) + I*1e-4;
            E = NR(diff_derivate_wrap, p, E0, dEb, h, 1);
            // Eb = bisezione(diff_derivate_wrap, p, dEb, E_old, E);
            printf("# \t-----> Stato legato: E* = %g+i%g <-----\n", P(E));

            //normalizzo ed esco
            cnormalizza(dx, phi);

            // per stampare lo stato
            if (atoi(argv[1]))
            {
                for (int i = 0; i < LEN; ++i)
                    // attenzione che phi e V sono complessi
                    printf("%g\t%g %g\t%g %g\n", x[i], P(phi[i]), P(V[i]));
            }

        // }

        // salvo i valori attuali
        // E_old = E; diff_old = dE;
    // }

    free(x); free(V); free(BC1); free(BC2); free(phi);

    return 0;
}
